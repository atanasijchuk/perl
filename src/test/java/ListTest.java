import com.codeborne.selenide.*;
import org.junit.Before;
import org.junit.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ListTest {
    private final String URL = "https://www.perlinka.ua/index.php/component/virtuemart/view/category/category_id/";
    @Before
    public void SetUp() {
        Configuration.browser = "chrome";
        Configuration.screenshots = false;
        Configuration.savePageSource = false;
    }

    //Проверка цены в карточке товара 3-х рандомных товаров на странице
    @Test
    public void CheckPrice() {
        int count = 38;
        for (int i = 0; i < 3; i++) {
            int random_number = (int) (Math.random() * count);
            Selenide.open(URL+"8/s/rasprodazha");
            $$(".items-list .item").get(random_number).click();
            $(".item-details__new-price").should(Condition.exist);
        }
    }
    //Проверка наличия цены всех товаров на страницы
    @Test
    public void CheckAllPrice() {
        Selenide.open(URL+"19/sku_type/1/s/tufli_dlya_devochki");
        for (int i = 0; i < 39; i++) {
            SelenideElement a =$$(".items-list .item .item__price").get(i).should(Condition.exist);
            System.out.println("Наличие цены "+i+" товара проверено");
        }
    }
    //Вывод "Номера заказа" товаров у которых название не содержит "Кроссовки"
    @Test
    public void OrderNumber () {
        Selenide.open(URL+"15/sku_type/1/s/demisezonnaya_obuv_dlya_malchika");
        for (int i = 0; i < 39; i++) {
          String order = $$(".items-list .item").excludeWith(Condition.text("Кроссовки")).get(i).$(".item__order-number").getText();
          System.out.println(i+" товара: "+order);
       }
    }
}
